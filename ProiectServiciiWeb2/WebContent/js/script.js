function schimba(numePagina)
{
	 var xhr = new XMLHttpRequest();
	  xhr.onreadystatechange = function() 
	  {
	    if (this.readyState == 4 && this.status == 200)
		{
	     document.getElementById("main").innerHTML = xhr.responseText;
	    }
	  };
	  xhr.open("GET", numePagina + ".jsp", true);
	  xhr.send();
}

function schimbaServlet(numePagina)
{
	 var xhr = new XMLHttpRequest();
	  xhr.onreadystatechange = function() 
	  {
		if (this.readyState == 4 && this.status == 200)
		{
		 document.getElementById("main").innerHTML = xhr.responseText;
		}
	  };
	  xhr.open("GET", numePagina, true);
	  xhr.send();
}