<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="content_box_genuriDrama">

	<h1 id="h_genuriDrama">Dramă</h1>

	<br>

	<p id="p_genuriDrama">În această secțiune puteți afla informații despre
		filmul pe care doriți să îl vizualizați. De asemenea vă sunt
		prezentate trailer-ele acestora.</p>

	<hr>

	<p id="p_NumeFilmeGenuriDrama">Titanic</p>

	<p id="p_genuriDrama">Brock Lovett este un căutător de comori și
		încearcă să recupereze vechi artefacte împreună cu echipa sa de pe
		epava Titanicului, un vapor uriaș care s-a scufundat în Oceanul
		Atlantic în 1912, după ce s-a izbit de un iceberg. Lovett caută
		diamante și pietre prețioase, dar descoperă un portret al unei tinere
		fete care nu poartă altceva decât un colier. Când Rose, în vârstă de
		102 ani, spune că ea este tânăra din portret, acțiunea se mută în 1912
		în Southampton, unde pasagerii se urcau la bordul navei Titanic.
		Printre ei se aflau tânărul Jack Dawson și Rose DeWitt Bukater, care
		se întorcea în Philadelphia cu logodnicul ei, Cal Hockley, un înstărit
		om de afaceri. În timpul călătoriei, Rose se îndrăgostește de Jack,
		iar Cal încearcă să se răzbune. Între timp, Titanicul se ciocnește de
		un iceberg și apa năvălește în compartimentele din față. Cal continuă
		să-i urmărească pe Jack și pe Rose, iar în tot acest timp uriașa navă
		își începe coborârea către adâncurile oceanului.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/ZQ6klONCq4s" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriDrama">The Shawshank Redemption</p>

	<p id="p_genuriDrama">Acțiunea filmului "Închisoarea îngerilor"
		începe in anul 1947, când Andy Dufresne, un tânăr bancher, primește
		dubla condamnare la închisoare pe viață pentru uciderea cu sânge rece
		a soției sale și a amantului acesteia, chiar dacă Andy susține că este
		nevinovat. Andy este trimis la închisoarea Shawshank, un loc ce îi va
		fi cămin pentru mulți ani.Aici el îl cunoaste pe Red, un negru închis
		de 20 ani. Acesta este un vulpoi bătrân și știe cum să procedeze în
		anumite situații imprevizibile din închisoare și de cine să se
		ferească. Andy se atașează de Red și cei doi devin prieteni foarte
		buni. în primii ani de ispășire a pedepsei, Andy duce o viață grea și
		mereu trebuie să se ferească de "Surori" - deținuții care îi agresează
		sexual pe ceilalți.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/NmzuHjWmXOc" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriDrama">The Notebook</p>

	<p id="p_genuriDrama">Allie Hamilton și Noah Calhoun, personajele
		principale ale unei cărți pe care un bătrân vine în fiecare zi să o
		citească unei femei internată într-un azil, se întâlnesc la un
		carnaval și se îndrăgostesc. Povestea lor de dragoste este plină de
		peripeții, începând cu dezacordul părinților lui Allie de a fi
		împreună cu Noah. Văzând că nu se pot impune în fața tinerilor,
		părinții hotărăsc ca familia să se mute din oraș. Neprimind nici un
		semn din partea lui Noah, Allie vrea să se căsătorească cu Lon, un
		soldat american, dar nu înainte de a merge să-l vadă pentru ultima
		dată pe Noah. Acum este momentul în care trebuie să decidă ce va face
		în continuare, simțind că dragostea dintre ea și Noah e la fel de
		puternică chiar și după atâția ani.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/EemLsTG5fX8" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriDrama">Hachiko: A Dog's Story</p>

	<p id="p_genuriDrama">Remake al filmului japonez Hachiko
		Monogatari, din anul 1987, și inspirată dintr-o poveste reală,
		petrecută în Japonia în prima parte a secolului al XX-lea, drama
		Hachiko: A Dog's Story urmărește relația specială, apărută spontan
		între un profesor universitar și un câine pe care îl găsește pe stradă
		și îl duce în casa lui. Ani de-a rândul, loialul animal Hachiko își
		însoțește în fiecare dimineață stăpnâul la stația de cale ferată și îl
		așteaptă în fiecare seară la aceeași oră și în același loc să se
		întoarcă de la serviciu. Într-o zi nefastă, însă, profesorul moare
		subit în urma unui atac de cord. Timp de 9 ani, Hachiko merge zi de zi
		la gară pentru a-și întâmpina stăpânul, uimindu-i pe localnici prin
		devotamentul lui și predându-le acestora o lecție despre iubire,
		loialitate și compasiune.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/Y6U7mAnPtw4" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriDrama">The Pianist</p>

	<p id="p_genuriDrama">Bazat pe memoriile pianistului Wladyslaw
		Szpilman, filmul prezintă povestea cutremurătoare a unui strălucit
		pianist polonez care, datorită originii sale evreiești, în timpul
		ocupației naziste a Poloniei duce o viață de fugar, pentru a nu fi
		deportat. Szpilman reușește să fugă din ghetoul din Varșovia și ajunge
		să se ascundă mult timp printre ruinele orașului. Salvarea îi vine de
		la un ofițer german care, impresionat de talentul lui, în loc să îl
		aresteze (sau chiar să îl omoare) nu destăinuie nimănui locul unde
		pianistul își găsise ascunzătoare.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/BFwGqLa_oAo" frameborder="0"
		allowfullscreen></iframe>

	<br> <br>
	<p id="multumire">Vă mulțumim pentru alegerea făcută !</p>

</div>
<!--Aici se incheie content box-->