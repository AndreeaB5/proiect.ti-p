<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div id="content_box_feedBack">

	<h1 id="h_feedBack">FeedBack</h1>

	<p id="p_feedBack">În această secțiune vă puteți exprima opinia în
		legătură cu serviciile oferite de noi.</p>

	<hr>

	<p id="p_feedBack">Așteptăm cu interes feedback-ul dumneavoastră cu
		privire la colaborarea avută cu noi deoarece părerile clienților ne
		pot ajuta să îmbunătățim calitatea serviciilor oferite.</p>

	<hr>

	<p id="p_feedBack">De asemenea, puteți fi de ajutor pentru
		persoanele ce doresc să apeleze la serviciile noastre, însa nu știu ce
		să aleagă. Vă mulțumim pentru colaborare și pentru încrederea oferită!</p>

	<hr>

	<br><br>
	
	 <span>
	 	<img id="feedBack_img_good" src="css/imagini/good.jpg" alt="Nu există." />
		<a id="feedBack_spre_propuneri" href="#" onclick="schimba('feedBackPropuneri')">Propuneri</a>
	    <img id="feedBack_img_bad" src="css/imagini/bad.jpg" alt="Nu există." />
	    <a id="feedBack_spre_reclamatii" href="#" onclick="schimba('feedBackReclamatii')">Reclamații</a>
     </span>
	
	<br><br><br><br><br><br><br><br>
		
	<p id="multumire_feedBack">Vă mulțumim pentru alegerea făcută !</p>

</div>
<!--Aici se incheie content box-->