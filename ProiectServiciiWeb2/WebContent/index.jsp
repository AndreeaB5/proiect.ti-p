<!DOCTYPE html>
<html>

	<head>
		<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
		<title>Popular Cinema</title>
		<link type="text/css" rel="stylesheet" href="css/style1.css"/>
		
		
		<script>
			function schimba(numePagina)
			{
				 var xhr = new XMLHttpRequest();
				  xhr.onreadystatechange = function() 
				  {
					if (this.readyState == 4 && this.status == 200)
					{
					 document.getElementById("main").innerHTML = xhr.responseText;
					}
				  };
				  xhr.open("GET", numePagina+".jsp", true);
				  xhr.send();
			}
			
			function schimbaServlet(numePagina)
			{
				 var xhr = new XMLHttpRequest();
				  xhr.onreadystatechange = function() 
				  {
					if (this.readyState == 4 && this.status == 200)
					{
					 document.getElementById("main").innerHTML = xhr.responseText;
					}
				  };
				  xhr.open("GET", numePagina, true);
				  xhr.send();
			}
</script>
		
	</head>
	
	<body>	
		
		<nav id="bara_navigatie">
			<ul id="menu">
					<li><a href="#" onclick="schimba('acasa')">Acasă</a></li>
					
					<li class="dropdown"> <a class="dropbtn" href="#" onclick="schimba('filme')">Filme<span>&nbsp;<img id="littleArrow" src="css/imagini/downButtonWhite6.png" alt="Nu exista"/></span></a>
						<div class="dropdown-content">
							<a href="#" onclick="schimbaServlet('MyServletFilme')">În cinematograf</a>
							<a href="#" onclick="schimba('filmeInCurand')">În curând</a>
						</div>
					</li>	
						
					<li class="dropdown"> <a class="dropbtn" href="#" onclick="schimba('genuri')">Genuri<span>&nbsp;<img id="littleArrow" src="css/imagini/downButtonWhite6.png" alt="Nu exista"/></span></a>
						<div class="dropdown-content">
							<a href="#" onclick="schimba('genuriActiune')">Acțiune</a>
							<a href="#" onclick="schimba('genuriComedie')">Comedie</a>
							<a href="#" onclick="schimba('genuriDrama')">Dramă</a>
							<a href="#" onclick="schimba('genuriHorror')">Horror</a>
							<a href="#" onclick="schimba('genuriSF')">Science-Fiction</a>
						</div>
					</li>
					
					<li class="dropdown"> <a class="dropbtn" href="#" onclick="schimba('feedBack')">FeedBack<span>&nbsp;<img id="littleArrow" src="css/imagini/downButtonWhite6.png" alt="Nu exista"/></span></a>
						<div class="dropdown-content">
							<a href="#" onclick="schimba('feedBackPropuneri')">Propuneri</a>
							<a href="#" onclick="schimba('feedBackReclamatii')">Reclamații</a>
						</div>
					</li>
					
					<li class="dropdown"> <a class="dropbtn" href="#" onclick="schimba('galerieFoto')">Galerie Foto</a></li>
					
					<li class="dropdown"> <a class="dropbtn" href="#" onclick="schimbaServlet('MyServletOraFilme')">Program</a></li>
					
					<li class="dropdown"> <a class="dropbtn" href="#" onclick="schimba('contact')">Contact</a></li>

				</ul>
		</nav>
			
			<br><br><br><br><br><br><br><br>
			
<section id="main">  <!--In loc de urmatorul text vor aparea celelalte sectiuni.--> 

			<div id="content_box">
							
								<h1 id="titlu_sectiune"> Bine ați venit la Popular Cinema! </h1>
								
								<br><br>
								
								<p id="p_index">Popular Cinema este un brand românesc. Este o destinație de distracție și un
								univers în care vei putea viziona filmele momentului, vei călatori în marile capitale ale lumii
								și vei asista la spectacole unice găzduite de cele mai proeminente scene, vei intra
								într-un spațiu creat pentru tine și cei dragi, unde vedeta ești tu și invitații tăi.</p>
								
								<hr>
								
								<p id="p_index">Popular Cinema este mai mult decât un cinematograf multiplex. Este tehnologie şi distracţie sub acelaşi acoperiş! Pe lângă cele mai noi filme ale momentului, 
								 îți aducem tehnologie de ultimă generație, dar şi multe evenimente speciale
								care să te bucure, să te inspire şi să te incite la distracţie, cultură şi artă.</p>
								
								<hr>
								
								<p id="p_index">Îți oferim pachete speciale în funcție de ofertele prezentate.
								Pentru că punem preț pe timpul liber petrecut într-o ambianță aparte, pe divertismentul de calitate și pe buna dispoziție.</p>
								
								<hr>
								
								<p id="p_index">Fiecare întrebare legată de politici sau rezervarea biletelor
								dumneavoastră își are răspunsul în experiența noastră. </p>
								
								<hr>
								
								<p id="p_index">Când vine vorba de un eveniment reușit, noi găsim soluția perfectă
								în funcție de bugetul dumneavoastră și putem oferi sfaturi practice pentru
								un plan perfect. </p>

								<hr>
						
						<br><br>

						<p id="multumire">Vă mulțumim pentru alegerea făcută !</p>
			</div>
					
</section>
			
			<br><br>
			
			
			<div id="chenar_footer">
				<footer>&copy;  ~Management cinematograf~ Proiect TI-P 2017 ~Cotilici Ana-Maria & Bordeianu Andreea & Bădărău Vlăduț~ </footer>
			</div>
	</body>	
</html>