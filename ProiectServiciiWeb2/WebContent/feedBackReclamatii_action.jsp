<!DOCTYPE html>
<html>
	<head>
		<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
		<%@ page import="java.io.*" %>
		
		<title>Popular Cinema</title>
		<link type="text/css" rel="stylesheet" href="css/style1.css"/>


<script>
	function schimba(numePagina) {
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById("main").innerHTML = xhr.responseText;
			}
		};
		xhr.open("GET", numePagina + ".jsp", true);
		xhr.send();
	}
</script>


</head>
	
	<body>	
		
		<nav id="bara_navigatie">
			<ul id="menu">
					<li><a href="#" onclick="schimba('acasa')">Acasă</a></li>
					
					<li class="dropdown"> <a class="dropbtn" href="#" onclick="schimba('filme')">Filme<span>&nbsp;<img id="littleArrow" src="css/imagini/downButtonWhite6.png" alt="Nu exista"/></span></a>
						<div class="dropdown-content">
							<a href="#" onclick="schimba('filmeInCinematograf')">În cinematograf</a>
							<a href="#" onclick="schimba('filmeInCurand')">În curând</a>
						</div>
					</li>	
						
					<li class="dropdown"> <a class="dropbtn" href="#" onclick="schimba('genuri')">Genuri<span>&nbsp;<img id="littleArrow" src="css/imagini/downButtonWhite6.png" alt="Nu exista"/></span></a>
						<div class="dropdown-content">
							<a href="#" onclick="schimba('genuriActiune')">Acțiune</a>
							<a href="#" onclick="schimba('genuriComedie')">Comedie</a>
							<a href="#" onclick="schimba('genuriDrama')">Dramă</a>
							<a href="#" onclick="schimba('genuriHorror')">Horror</a>
							<a href="#" onclick="schimba('genuriSF')">Science-Fiction</a>
						</div>
					</li>
					
					<li class="dropdown"> <a class="dropbtn" href="#" onclick="schimba('feedBack')">FeedBack<span>&nbsp;<img id="littleArrow" src="css/imagini/downButtonWhite6.png" alt="Nu exista"/></span></a>
						<div class="dropdown-content">
							<a href="#" onclick="schimba('feedBackReclamatii')">Propuneri</a>
							<a href="#" onclick="schimba('feedBackReclamatii')">Reclamații</a>
						</div>
					</li>
					
					<li class="dropdown"> <a class="dropbtn" href="#" onclick="schimba('galerieFoto')">Galerie Foto</a></li>
					
					<li class="dropdown"> <a class="dropbtn" href="#" onclick="schimba('programCinema')">Program</a></li>
					
					<li class="dropdown"> <a class="dropbtn" href="#" onclick="schimba('contact')">Contact</a></li>

				</ul>
		</nav>
			
			<br><br><br><br><br><br><br><br>

<section id="main">  <!--In loc de urmatorul text vor aparea celelalte sectiuni.--> 

<div id="content_box_feedBackReclamatii_action">

        <h1 id="titlu_sectiune">Reclamația a fost trimisă cu succes. Vă mulțumim!</h1>
        
        <br>
        
        <img id="feedBackReclamatii_action_img_successfully" src="css/imagini/transparent_message_sent.jpg" alt="Nu există." />
        
        <%
	        String name = request.getParameter("nume");
	        String coment = request.getParameter("comentariu");
	        String file = application.getRealPath("/") + "reclamatii.json";
	        FileWriter filewriter = new FileWriter(file, true);
	        filewriter.write("{\"Nume\""+":"  + "\""+name+"\"" + "}");
	        filewriter.write("\n");
	        filewriter.write("{\"Comentariu\""+":"  + "\""+coment+"\"" + "}");
	        filewriter.write("\n");
	        filewriter.write("\n");
	        filewriter.close();
        %>
</div>        
</section>

		<div id="chenar_footer">
			<footer>&copy;  ~Management cinematograf~ Proiect TI-P 2017 ~Cotilici Ana-Maria & Bordeianu Andreea & Bădărău Vlăduț~ </footer>			</div>
		</div>
			
	</body>	
</html>