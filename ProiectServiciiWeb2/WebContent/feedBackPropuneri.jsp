<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div id="content_box_feedBackPropuneri">
			
		<h1 id="h1_feedBackPropuneri">Propuneri</h1>
					
			<h2 id="h2_feedBackPropuneri">Aici puteți introduce propunerile dumneavoastră!</h2>
			
			<br><br><br><br>
			
            <form action="feedBackPropuneri_action.jsp" method="POST">
	               <label id="feedBackPropuneri_label_nume"> Introduceți numele dumneavoastră:</label><br>
	               <br>
		           <input id="feedBackPropuneri_input" type="text" name="nume">
		           <br><br><br>
		              
	               <label id="feedBackPropuneri_label_comentariu">Introduceți comentariul dumneavoastră:</label><br>
	               <br>
	               <textarea id="feedBackPropuneri_textarea" name="comentariu" rows="5" cols="50"></textarea>
	               <br><br>
	               <input id="feedBackPropuneri_submitButton"  type="submit" value="Trimite">&nbsp;&nbsp;<input id="feedBackPropuneri_resetButton" type="reset" value="&nbsp;Reset&nbsp;">
            </form>

		<br><br>
		
		<p id="multumire_feedBackPropuneri">Vă mulțumim pentru alegerea făcută !</p>
	
</div><!--Aici se incheie content box-->