<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="content_box">

	<h1 id="titlu_sectiune">Bine ați venit la Popular Cinema!</h1>

	<br>
	<br>

	<p id="p_index">Cinema One este un brand românesc. Este o
		destinație de distracție și un univers în care vei putea viziona
		filmele momentului, vei călatori în marile capitale ale lumii și vei
		asista la spectacole unice găzduite de cele mai proeminente scene, vei
		intra într-un spațiu creat pentru tine și cei dragi, unde vedeta ești
		tu și invitații tăi.</p>

	<hr>

	<p id="p_index">Cinema One este mai mult decât un cinematograf
		multiplex. Este tehnologie şi distracţie sub acelaşi acoperiş! Pe
		lângă cele mai noi filme ale momentului, îți aducem tehnologie de
		ultimă generație, dar şi multe evenimente speciale care să te bucure,
		să te inspire şi să te incite la distracţie, cultură şi artă.</p>

	<hr>

	<p id="p_index">Îți oferim pachete speciale în funcție de ofertele
		prezentate. Pentru că punem preț pe timpul liber petrecut într-o
		ambianță aparte, pe divertismentul de calitate și pe buna dispoziție.</p>

	<hr>

	<p id="p_index">Fiecare întrebare legată de politici sau rezervarea
		biletelor dumneavoastră își are răspunsul în experiența noastră.</p>

	<hr>

	<p id="p_index">Când vine vorba de un eveniment reușit, noi găsim
		soluția perfectă în funcție de bugetul dumneavoastră și putem oferi
		sfaturi practice pentru un plan perfect.</p>

	<hr>

	<br>
	<br>

	<p id="multumire">Vă mulțumim pentru alegerea făcută !</p>
</div>

