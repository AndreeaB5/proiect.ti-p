<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="content_box_contact">

	<h1 id="h_contact">Informații de contact</h1>

	<br><br>

	<p id="p_informatii_contact">Vă punem la dispoziție informațiile
		necesare, astfel încat să ne puteți contacta cât mai ușor.</p>

	<hr>

	<p id="p_informatii_contact">Fiecare întrebare legată de filme,
		rezervarea biletelor sau politici își are răspunsul în experiența
		noastră.</p>

	<br><br>
	
	<table id="tabel_contact" border="1">

		<tr>
			<td>Adresă:</td>
			<td>Tudor Vladimirescu, Iași, Iulius Mall</td>
		</tr>

		<tr>
			<td>Nr.Tel:</td>
			<td>Mobil:0265791264</td>
		</tr>

		<tr>
			<td>Email:</td>
			<td>contact@popular_cinema.ro</td>
		</tr>

		<tr>
			<td>Social:</td>
			<td>Facebook, Twitter, Instagram</td>
		</tr>

		<tr>
			<td>Orar:</td>
			<td>Luni-Vineri: 12:00-23:00</td>
		</tr>

	</table>

	<br><br><br>
	
	<p id="multumire">Vă mulțumim pentru alegerea făcută !</p>

</div>
<!--Aici se incheie content box-->