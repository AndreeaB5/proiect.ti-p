<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="content_box_genuriHorror">

	<h1 id="h_genuriHorror">Horror</h1>

	<br>

	<p id="p_genuriHorror">În această secțiune puteți afla informații despre
		filmul pe care doriți să îl vizualizați. De asemenea vă sunt
		prezentate trailer-ele acestora.</p>

	<hr>

	<p id="p_NumeFilmeGenuriHorror">The Conjuring</p>

	<p id="p_genuriHorror">Înainte de Amityville, a fost Harrisville.
		Renumiţii specialişti în paranormal, Ed şi Lorraine Warren sunt
		chemaţi de o familie ce s-a stabilit recent la o fermă retrasă.
		Familia apelează la ajutorul şi experienţa lor pentru a scăpa de
		prezenţa întunecată care îi terorizează. Constrânşi să înfrunte
		entitatea demonică, soţii Warren se văd prinşi în cel mai îngrozitor
		caz din viaţa lor.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/ejMMn0t58Lc" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriHorror">The Ring</p>

	<p id="p_genuriHorror">O casetă video îi sperie pe oamenii unui
		oraș de pe coasta de vest. Caseta conține o serie de imagini ciudate
		și tulburătoare și, după ce le vizionează, mulți oameni primesc un
		telefon necunoscut prin care sunt averizați că vor muri în șapte zile.
		Câțiva tineri care vizionează caseta în timpul unei mini-vacanțe la o
		cabană primesc apelul cu averizarea, însă nu-l iau în seamă și toți
		mor în aceeași noapte. Rachel Keller este un jurnalist care s-a decis
		să investigheze problema. Ea găsește cabana din munți și vizionează
		caseta. După ce primește apelul, Rachel își da seama că trebuie să
		rezolve cazul în mai puțin de o săptămână.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/_PkgRhzq_BQ" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriHorror">Mirrors</p>

	<p id="p_genuriHorror">Remake al unui film japonez, "Mirrors" este
		un horror terifiant ce îl are în centrul acțiunii pe Ben Carson, un
		fost polițist care lucrează acum ca agent de pază la un imens magazin
		universal, închis multă vreme din cauza unui incediu devastator în
		care și-au pierdut viața foarte mulți angajați. În scurt timp, Ben își
		dă seamă că magazinul este ținta demonstranților care doresc
		compensații materiale pentru daunele suferite în timpul cumplitei
		tragedii și totodată locul în care se petrec decese inexplicabile și
		în care pare să bântuie un spirit malefic. Bărbatul este intrigat de o
		femeie necunoscută care pretinde că fantoma surorii ei, moartă în
		incendiul respectiv, s-a întors pe pământ dornică de răzbunare și se
		folosește de oglinzile magazinului că de niște veritabile porți de
		intrare în lumea celor vii.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/O92QxxgeCO8" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriHorror">Get Out!</p>

	<p id="p_genuriHorror">Chris (Daniel Kaluuya, Sicario) și iubita
		sa, Rose (Allison Williams, Girls), au ajuns în relația lor la etapa
		în care acesta urmează să cunoască părinții fetei, pe Missy (Catherine
		Keener, Captain Phillips) și Dean (Bradley Whitford, The Cabin in the
		Woods). La început, Chris crede că amabilitatea exagerată a părinților
		fetei este doar emoție provocată de eforturile acestora de a se
		obișnui cu idea că aceasta e într-o relație interrasială, dar, pe
		măsură ce trece timpul, o serie de descoperiri bizare și din ce în ce
		mai tulburătoare îi dezvăluie un adevăr atât de cumplit cum nu și-ar
		fi putut imagina vreodată.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/DzfpyUB60YY" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriHorror">Oculus</p>

	<p id="p_genuriHorror">O femeie încearcă să-şi scape fratele de la
		închisoare şi să-i dovedească nevinovăţia. El a fost acuzat şi
		condamnat pentru crimă, dar sora lui vrea să demonstreze că acea
		moarte a fost cauzată de un fenomen supranatural.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/dYJrxezWLUk" frameborder="0"
		allowfullscreen></iframe>

	<br> <br>
	<p id="multumire">Vă mulțumim pentru alegerea făcută !</p>

</div>
<!--Aici se incheie content box-->