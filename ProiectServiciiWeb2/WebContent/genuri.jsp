<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="content_box_genuri">

	<h1 id="h_genuri">Genuri</h1>

	<p id="p_genuri">În această secțiune puteți vizualiza descrierile și trailer-ele
			filmelor din cinematograf, în funcție de categorie.</p>
			
	<hr>
	
	<br>
	
	<p id="p_genuri">Puteți selecta una dintre categoriile următoare:</p>
	
	<br>
	
	<span id="span_genuri">
		<a id="genuri_spreCategorii" href="#" onclick="schimba('genuriActiune')">Acțiune</a>
		<a id="genuri_spreCategorii" href="#" onclick="schimba('genuriComedie')">Comedie</a>
		<a id="genuri_spreCategorii" href="#" onclick="schimba('genuriDrama')">Dramă</a>
		<a id="genuri_spreCategorii" href="#" onclick="schimba('genuriHorror')">Horror</a>
		<a id="genuri_spreCategorii" href="#" onclick="schimba('genuriSF')">Science-Fiction</a>
	</span>
	
	<br><br><br><br><br><br>
	
	<p id="multumire">Vă mulțumim pentru alegerea făcută !</p>

</div>
<!--Aici se incheie content box-->


