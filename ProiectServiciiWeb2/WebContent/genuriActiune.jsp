<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="content_box_genuriActiune">

	<h1 id="h_genuriActiune">Acțiune</h1>

	<br>

	<p id="p_genuriActiune">În această secțiune puteți afla informații despre
		filmul pe care doriți să îl vizualizați. De asemenea vă sunt
		prezentate trailer-ele acestora.</p>

	<hr>

	<p id="p_NumeFilmeGenuriActiune">Inception</p>

	<p id="p_genuriActiune">Dom Cobb este un hoț experimentat, cel mai
		bun în arta periculoasă de extracție, furtul secretelor valoroase
		stocate adânc în subconștient în timpul stării de vis, atunci când
		mintea este cea mai vulnerabilă. Capacitatea lui Cobb a făcut din el
		un jucător râvnit în această nouă lume înselătoare de spionaj
		corporativ, dar de asemenea a făcut din el un fugar internațional și
		l-a costat tot ce a iubit. Acum lui Cobb îi este oferită o sansă de
		răscumparare. Un ultim loc de muncă ar putea să-i dea viața înapoi,
		dar numai dacă el poate realiza imposibilul numit Inception (început).
		În locul unui jaf perfect, Cobb și echipa sa de specialiști trebuie să
		obțină opusul: sarcina lor nu este sa extragă o idee, ci să o
		planteze. Dacă reușesc, ar putea fi o crimă perfectă. Dar nici un fel
		de planificare atentă sau expertiză nu poate pregăti echipa pentru un
		inamic periculos, care pare să prezică fiecare mișcare a lor. La un
		asemenea dușman numai Cobb putea să se astepte pentru că el e singurul
		a cărui minte funcționează la fel de complex.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/YoHD9XEInc0" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriActiune">Logan</p>

	<p id="p_genuriActiune">Suntem în 2029. Mutanţii au dispărut — sau
		sunt pe cale să dispară. Un Logan izolat, deznădăjduit, care îşi
		pierde zilele bând, ascunzându-se într-o zonă îndepărtată a graniţei
		cu Mexicul, făcând rost de mărunţiş lucrând ca șofer. Companionii lui
		în exil sunt proscrisul Caliban şi un Profesor X suferind, a cărui
		minte singulară este îmbolnăvită de crize care se înrăutăţesc. Dar
		încercarea lui Logan de a se ascunde de lume şi de moştenirea sa ia
		sfârșit brusc când o femeie misterioasă apare cu o solicitare urgentă
		— ca Logan să aibă grijă de o tânără extraordinară, astfel încât ca
		aceasta să fie în siguranţă. Curând, se vor produce încleştări,
		deoarece Logan trebuie să facă faţă forţelor întunecate şi unui
		ticălos din trecutul său într-o misiune pe viaţă şi pe moarte, misiune
		care îl va direcţiona pe luptătorul uitat de vreme către un drum ce îl
		va conduce spre împlinirea destinului său.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/DekuSxJgpbY" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriActiune">Terminator</p>

	<p id="p_genuriActiune">John Connor (Jason Clarke), liderul
		rezistenței oamenilor, îl trimite pe sergentul Kyle Reese (Jai
		Courtney) înapoi în 1984 ca să o protejeze pe Sarah Connor (Emilia
		Clarke) și să se asigure că omenirea va avea un viitor pașnic. Însă
		lucrurile iau o întorsătură neașteptată și se creează o fractură în
		linia temporală. Astfel, Reese se trezește într-o versiune nouă a
		trecutului care îi este necunoscută, unde are aliați surprinzători,
		inclusiv pe Gardian (Arnold Schwarzenegger), inamici periculoși, dar
		și o nouă misiune: să reseteze viitorul.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/jNU_jrPxs-0" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriActiune">Taken</p>

	<p id="p_genuriActiune">Ce poate fi mai rău pentru un tată decât să
		asiste neputincios, prin telefon, la răpirea fiicei sale? Acesta este
		coșmarul trăit de Bryan, fost agent secret, care are la dispoziție
		doar câteva ore pentru a o salva pe fiica sa, Kim, din mâinile unei
		bande de infractori specializați în trafic de carne vie. Panica crește
		datorită unui obstacol fundamental: Bryan se află în Los Angeles, iar
		Kim în Paris...</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/uPJVJBm9TPA" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriActiune">Deadpool</p>

	<p id="p_genuriActiune">Supranumit "mercenarul cu gură mare",
		Deadpool obține, în urma unui experiment militar care dorea să redea
		abilitatea de regenerare rapidă a lui Wolverine, puteri incredibile
		(invulnerabilitatea la puteri telepatice și telekinezice, rezistența
		la droguri și toxine, dar și prelungirea vieții grație puterilor de
		refacere ale organismului său). Acestea vin, însă, și cu unele
		inconveniente: instabilitatea psihică și un fizic desfigurat.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/Xithigfg7dA" frameborder="0"
		allowfullscreen></iframe>

	<br> <br>
	<p id="multumire">Vă mulțumim pentru alegerea făcută !</p>

</div>
<!--Aici se incheie content box-->