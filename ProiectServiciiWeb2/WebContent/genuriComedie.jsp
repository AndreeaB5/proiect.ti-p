<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="content_box_genuriComedie">

	<h1 id="h_genuriComedie">Comedie</h1>

	<br>

	<p id="p_genuriComedie">În această secțiune puteți afla informații despre
		filmul pe care doriți să îl vizualizați. De asemenea vă sunt
		prezentate trailer-ele acestora.</p>

	<hr>

	<p id="p_NumeFilmeGenuriComedie">We're the Millers</p>

	<p id="p_genuriComedie">David Clark (Jason Sudeikis) este un mărunt
		traficant de droguri care își câștigă existența vânzând droguri unor
		oameni de afaceri și casnice plictisite, dar niciodată copiilor. Deci,
		ce ar putea merge rău? Multe. Învață cum nu se poate mai greu că nicio
		faptă bună nu trece nepedepsită. Atunci când încearcă să ajute câțiva
		adolescenți, acesta este jefuit de trei tipi care îi fură toți banii
		și toată marfa, lăsându-l cu mari datorii la distribuitorul său, Brad
		(Ed Helms). Pentru a scăpa de datorii și a-și salva viața, David
		trebuie acum să introducă în țară, din Mexic, o cantitate foarte mare
		de droguri. De aceea, se gândește la un plan infailibil și își
		forțează ciudații vecini să intre în joc. Astfel, cinica stripteuză
		Rose (Jennifer Aniston), rebela Casey (Emma Roberts) și timidul Kenny
		(Will Poulter) devin falsa familie a lui David. Cu o soție, doi copii
		și o rulotă imensă, noua familie Miller, în frunte cu David, se
		îndreaptă spre graniță pentru a sărbători Ziua Națională într-un
		weekend imprevizibil peste hotare.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/0Vsy5KzsieQ" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriComedie">The Proposal</p>

	<p id="p_genuriComedie">Când Margaret, un important editor de carte
		din New York riscă să fie repatriată în Canada sa natală, obișnuită să
		gândească rapid declară că este de fapt logodită cu asistentul ei,
		Andrew, care nu bănuiește nimic și pe care îl chinuie de mai mulți
		ani. El este de acord să îi facă jocul, dar impune câteva condiții
		proprii. Surprinzătorul cuplu se îndreaptă către Alaska, pentru a
		cunoaște capricioasa familie a lui Andrew, iar tânăra femeie care la
		oraș deținea întotdeauna controlul se trezește prinsă într-un șir de
		situații comice tipice, ca de "pește pe uscat". Pregătind o nuntă
		neașteptată și urmăriți de un ofițer al Biroului de Imigrare, Margaret
		și Andrew acceptă fără prea multă tragere de inimă să respecte acest
		plan, în ciuda consecințelor sale.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/RFL8b1p1ELY" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriComedie">Meet the Parents</p>

	<p id="p_genuriComedie">Greg Focker (Ben Stiller) este îndrăgostit
		până peste cap de prietena sa Pam (Teri Polo) și este pe punctul de
		a-i pune întrebarea cheie "Vrei să fii soția mea?". Când și-a propus
		să o facă este însă întrerupt de un telefon în care Pam este anunțată
		de sora ei mai mică de faptul că a fost cerută în căsătorie și că se
		mărită. Greg își dă seama că trebuie să o ceară pe Pam tatălui ei și
		că ocazia nunții surorii ei ar fi potrivită pentru cererea mâinii
		iubitei sale. Ajunge la familia Byrnes - care pare ideală - în prag -
		un soț și tată iubitor (Robert De Niro), o mamă la fel și o pisică.
		Greg se dă peste cap să se faca plăcut dar weekend-ul debutează
		catastrofal- bagajul îi este rătăcit la aeroport și alunecă într-o
		cascadă de întâmplări hazlii dar și dezastruoase.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/CXqxP-bUC7I" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriComedie">Big Momma's House</p>

	<p id="p_genuriComedie">Martin Lawrence joacă în această comedie
		rolul unui polițist țăcănit care ia identitatea unei bunici
		supraponderale pentru a rezolva un caz complicat. Maestru în arta
		travestirilor, agentul FBI Malcolm Turner trebuie să captureze un
		infractor foarte periculos evadat din închisoare. Pentru a pune mâna
		pe el, Turner se deplasează în orășelul în care locuiește frumoasa
		prietenă a fugitivului. Acolo, închiriază o casă și ia înfățișarea lui
		Big Momma. Excentrică și cu limba ascuțită ea va deveni, peste noapte,
		o adevărată celebritate a orășelului. Casa ei va deveni loc de
		peregrinare pentru toți petrecăreții. Turner nu va renunța la
		travestire până nu-l va descoperi pe făptaș. Stilul lui Lawrence,
		multitudinea de personaje bizare precum și răsturnările situației au
		făcut din "Big Momma's House" un film extrem de amuzant.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/e3J29PNvIhY" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriComedie">Liar, Liar</p>

	<p id="p_genuriComedie">Fletcher Reede (Jim Carrey) este un avocat
		obsedat de munca lui care își câștigă existența mințind pe toată
		lumea. Acest prost obicei l-a transformat într-un om prosper, dar îl
		îndepărtează în timp de soția lui Audrey (Maura Tierney) și începe
		să-i afecteze și relația cu fiul său, Max (Justin Cooper). Deși
		Fletcher i-a promis de mai multe ori lui Max că va veni la aniversarea
		lui de opt ani, în momentul în care apare ceva important la muncă,
		uită cu desăvârșire de petrecere. Într-un târziu sună acasă și se
		scuză într-un mod atât de penibil, încât chiar și fiul lui iși dă
		seama că tatăl său este cel mai mare mincinos de pe pământ. Când vine
		timpul să sufle în lumânări, Max își pune o dorință: ca tatăl lui să
		nu mai poată spune nici o minciună pentru o zi întreagă. În mod
		miraculos, dorința micuțului se îndeplinește și a doua zi, marele
		avocat descoperă că este total incapabil de a spune ceva ce nu este
		adevărat. Viața lui ia cu totul o altă turnură, în principal din cauza
		încurcăturilor în lanț atât în firma unde lucra cât și în procesul de
		divorț în care era implicat..</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/C1no75lpOiw" frameborder="0"
		allowfullscreen></iframe>

	<br> 
	<p id="multumire">Vă mulțumim pentru alegerea făcută !</p>

</div>
<!--Aici se incheie content box-->