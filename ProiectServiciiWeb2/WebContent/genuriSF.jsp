<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="content_box_genuriSF">

	<h1 id="h_genuriSF">Science-Fiction</h1>

	<br>

	<p id="p_genuriSF">În această secțiune puteți afla informații
		despre filmul pe care doriți să îl vizualizați. De asemenea, vă sunt
		prezentate trailer-ele acestora.</p>

	<hr>

	<p id="p_NumeFilmeGenuriSF">Interstellar</p>

	<p id="p_genuriSF">În contextul în care existenţa omenirii pe Terra
		este ameninţată, un grup de exploratori depăşeşte limitele spaţiului
		şi cucereşte imensele distanţe ale unei călătorii interstelare.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/zSWdZVtXT7E" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriSF">The Martian</p>

	<p id="p_genuriSF">În timpul unei misiuni umane pe planeta Marte,
		astronautul Mark Watney trece printr-o furtună puternică, e lăsat în
		urmă de echipă şi e considerat mort. Însă Watney a reuşit să
		supravieţuiască şi îşi dă seama că a rămas izolat pe o planetă ostilă.
		Şansele lui de a rezista şi de a trimite un semnal către Terra că e în
		viaţă depind de câteva provizii sărăcăcioase, de inventivitatea sa şi
		de cât de puternic îi e spiritul în faţa disperării.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/ej3ioOneTy8" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriSF">Gravity</p>

	<p id="p_genuriSF">Bullock va interpreta rolul lui Dr. Ryan Stone,
		un medic foarte bine calificat în domeniul bioingineriei aflat la
		prima sa misiune în spațiu alături de veteranul astronaut Matt
		Kowalsky (Clooney), care se află pentru ultima dată la comanda unei
		nave înainte de pensionare. Însă în timpul unei misiuni de rutină în
		afara navetei, se întâmplă un dezastru. Aceasta este distrusă,
		lăsându-i pe Stone și Kowalsky complet singuri - legați unul de altul
		și de nimic altceva, descriind spirale în întuneric. Tăcerea
		insuportabilă le spune că au pierdut orice legătură cu Pământul... și
		orice șansă de a fi salvați. Pe măsură ce frica se transformă în
		panică, cu fiecare gură de aer se consumă puținul oxigen rămas. Însă
		singura cale de a ajunge acasă este aceea de a înainta prin terifianta
		întindere a spațiului.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/OiTiKOy59o4" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriSF">Edge of Tomorrow</p>

	<p id="p_genuriSF">Într-un viitor nu foarte îndepărtat Pământul e
		atacat de Mimics, o rasă extraterestră care se comportă ca un stup şi
		care au redus oraşele la mormane de moloz şi au ucis milioane de
		oameni. Armatele din toate ţările îşi unesc eforturile într-o ultimă
		defensivă. Locotenent-colonelul Bill Cage nu a participat niciodată la
		lupte pe front, dar e trimis direct în bătaia armelor. În doar câteva
		minute el e ucis, dar reuşeşte să ia şi viaţa unui extraterestru
		alpha. Însă militarul se trezeşte la dimineaţa în aceeaşi zi pe care a
		trăit-o deja şi e iar trimis cu forţa să lupte şi să moară. Iar şi
		iar. Contactul fizic cu extraterestrul l-a aruncat pe Bill Cage într-o
		buclă a timpului care se repetă la infinit.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/vw61gCe2oqI" frameborder="0"
		allowfullscreen></iframe>

	<hr>

	<p id="p_NumeFilmeGenuriSF">Arrival</p>

	<p id="p_genuriSF">Au aterizat! Nave spațiale misterioase sunt deja
		răspândite pe 12 meridiane de pe toate continentele. Dar de ce au
		venit? Ce intenții au? Iată ce are de elucidat Louise Banks (Amy
		Adams), expertă în lingvistică, solicitată de autorități ca să
		descifreze limbajul "intrușilor". Termen: două zile. Miza: viața
		tuturor. Dușmani sau prieteni? Război inter-planetar sau șansa de a
		cunoaște alte forme de inteligență? Succes sau condamnare? O planetă
		întreagă tremură în așteptarea veștilor. Va reuși oare Louise să
		traducă la timp limbajul invadatorilor extratereștri sau va "traduce"
		soarta umanității? Inspirat de nuvela "Story of Your Life", semnată de
		Ted Chiang, filmul este regizat de cunoscutul regizor canadian Denis
		Villeneuve și a fost nominalizat la Festivalul de Film de la Veneția
		pentru Leul de Aur. În distribuție îi veți mai recunoaște pe Forest
		Whitaker și Jeremy Renner.</p>

	<iframe width="600" height="320"
		src="https://www.youtube.com/embed/AMgyWT075KY" frameborder="0"
		allowfullscreen></iframe>

	<br> <br>
	<p id="multumire">Vă mulțumim pentru alegerea făcută !</p>

</div>
<!--Aici se incheie content box-->