package servicii.web;

public class Filme
{
	private int ID_film;
	private String nume_film;
	private String gen_film;
	private int pret;

	public Filme(int ID_film, String nume_film, String gen_film, int pret) 
	{
		this.ID_film = ID_film;
		this.nume_film = nume_film;
		this.gen_film = gen_film;
		this.pret = pret;
	}
	public int getID_film() {
		return ID_film;
	}
	public String getNume() {
		return nume_film;
	}
	public String getGen() {
		return gen_film;
	}
	public int getPret() {
		return pret;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("<tr><td>").append(ID_film).append("</td> <td>").append(nume_film).append("</td> <td>")
		.append(gen_film).append("</td> <td>").append(pret).append("</td>").append("</tr>");
		return builder.toString();
	}
}