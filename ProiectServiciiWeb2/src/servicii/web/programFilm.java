package servicii.web;

import java.sql.Date;
import java.sql.Time;

public class programFilm
{
	private String nume_film;
	private Date data_rulare;
	private Time ora_rulare;

	public programFilm(String nume_film, Date data_rulare, Time ora_rulare) 
	{
		this.nume_film = nume_film;
		this.data_rulare = data_rulare;
		this.ora_rulare = ora_rulare;
	}
	
	public String getNume() {
		return nume_film;
	}
	public Date getData_Rulare() {
		return data_rulare;
	}
	public Time getOra_Rulare() {
		return ora_rulare;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("<tr><td>").append(nume_film).append("</td><td>").append(data_rulare).append("</td><td>").append(ora_rulare).append("</td></tr>");
		return builder.toString();
	}
}