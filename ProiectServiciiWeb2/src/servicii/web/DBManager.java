package servicii.web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DBManager {
	private static final String URL = "jdbc:mysql://localhost:3306/sys";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "Salamandra1";
	private static final DBManager instance = new DBManager();
	private Connection conn;
	public static DBManager getInstance() {
		return instance;
	}
	private DBManager() {
		System.out.println("Loading driver...");
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Driver loaded!");
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException(
					"Cannot find the driver in the classpath!", e);
		}
		try {
			conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<Filme> getFilmeList() {
		try (Statement st = conn.createStatement()) {
			List<Filme> filmeList = new ArrayList<Filme>();
			st.execute("select ID_film, nume_film, gen_film, pret from student");
			ResultSet rs = st.getResultSet();
			while (rs.next()) {
				Filme filme = new Filme(rs.getInt("ID_film"), rs.getString("nume_film"), rs.getString("gen_film"), rs.getInt("pret"));
				filmeList.add(filme);
			}
			// st.close();
			return filmeList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<programFilm> getProgramFilm() {
		try (Statement st = conn.createStatement()) {
			List<programFilm> ora_rulareList = new ArrayList<programFilm>();
			st.execute("select nume_film, data_rulare, ora_rulare from student");
			ResultSet rs = st.getResultSet();
			while (rs.next()) {
				programFilm ProgramFilm = new programFilm(rs.getString("nume_film"), rs.getDate("data_rulare"), rs.getTime("ora_rulare"));
				ora_rulareList.add(ProgramFilm);
			}
			// st.close();
			return ora_rulareList;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}