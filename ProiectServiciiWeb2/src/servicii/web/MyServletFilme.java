package servicii.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MyServlet
 */
@WebServlet("/MyServletFilme")
public class MyServletFilme extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyServletFilme() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String htmlStruct = "<div id=\"content_box_filmeInCinematograf\">" +
							"<h1 id=\"h_filmeInCinematograf\">Filme in cinematograf</h1>";
		
		String TableStart = "<table id=\"tabel_filmeInCinematograf\"> <tr> <th>ID</th><th>Nume</th> <th>Gen</th> <th>Pret</th> </tr>";
		
		String htmlDB =  DBManager.getInstance().getFilmeList().toString();
		
		String TableEnd = "</table>";
				
		String htmlEnd = "</div><!--Aici se incheie content box-->";
		
		response.getWriter().append(htmlStruct).append(TableStart).append(htmlDB).append(TableEnd).append(htmlEnd).append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
